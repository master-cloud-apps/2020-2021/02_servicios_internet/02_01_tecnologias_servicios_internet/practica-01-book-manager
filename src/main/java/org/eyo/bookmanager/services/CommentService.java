package org.eyo.bookmanager.services;

import org.eyo.bookmanager.models.Comment;

import java.util.Collection;

public interface CommentService {
    Collection<Comment> getAll(Long bookId);

    void save(Long bookId, Comment comment);

    void deleteComment(Long bookId, Long commentId);

    Comment finById(Long commentId);
}
