package org.eyo.bookmanager.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.eyo.bookmanager.models.Comment;
import org.eyo.bookmanager.services.BookService;
import org.eyo.bookmanager.services.CommentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

@RestController
@RequestMapping("/books/{bookId}/comments")
public class CommentController {

    private CommentService commentService;
    private BookService bookService;

    public CommentController(CommentService commentService, BookService bookService) {
        this.commentService = commentService;
        this.bookService = bookService;
    }

    @Operation(summary = "Create a comment")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Comment created",
                    headers = @Header(name = "Location", description = "Location of the comment", schema =
                    @Schema(implementation = String.class))
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request",
                    content = @Content
            )
    })
    @PostMapping("")
    public ResponseEntity<Void> createComment(@PathVariable Long bookId, @RequestBody Comment comment) {
        this.commentService.save(bookId, comment);

        URI location =
                fromCurrentRequest().path("/{id}").buildAndExpand(comment.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Get a comment by id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Comment selected",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = Comment.class))
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Comment not found",
                    content = @Content
            )
    })
    @GetMapping("/{commentId}")
    public ResponseEntity<Comment> getCommentById(@PathVariable Long bookId, @PathVariable Long commentId) {
        if (!existBookAndComment(bookId, commentId)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(this.commentService.finById(commentId));
    }

    @Operation(summary = "Delete a comment by id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Comment deleted"
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Book not found",
                    content = @Content
            )
    })
    @DeleteMapping("/{commentId}")
    public ResponseEntity<Void> deleteCommentOverBook(@PathVariable Long bookId, @PathVariable Long commentId) {
        if (!existBookAndComment(bookId, commentId)) {
            return ResponseEntity.notFound().build();
        }
        this.commentService.deleteComment(bookId, commentId);
        return ResponseEntity.noContent().build();
    }

    private boolean existBookAndComment(Long bookId, Long commentId) {
        return this.commentService.finById(commentId) != null && this.bookService.findById(bookId) != null;
    }
}
