package org.eyo.bookmanager.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.eyo.bookmanager.dtos.BookGet;
import org.eyo.bookmanager.models.Book;
import org.eyo.bookmanager.services.BookService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

@RestController
@RequestMapping("/books")
public class BookController {

    private BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @Operation(summary = "Create a book")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Book created",
                    headers = @Header(name = "Location", description = "Location of the book", schema =
                    @Schema(implementation = String.class))
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request",
                    content = @Content
            )
    })
    @PostMapping("")
    public ResponseEntity<Void> createBook(@RequestBody Book newBook) {
        this.bookService.save(newBook);

        URI location = fromCurrentRequest().path("/{id}").buildAndExpand(newBook.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Get all the books")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Returns all the books",
                    content = @Content(mediaType = "application/json", array = @ArraySchema(schema =
                    @Schema(implementation = Book.class)))
            )
    })
    @GetMapping("")
    public ResponseEntity<Collection<Book>> getBooks() {
        return ResponseEntity.ok(this.bookService.getAll());
    }

    @Operation(summary = "Delete a book by id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Book deleted"
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Book not found",
                    content = @Content
            )
    })
    @DeleteMapping("/{bookId}")
    public ResponseEntity<Void> deleteBookById(@PathVariable Long bookId) {
        Book bookToDelete = this.bookService.findById(bookId);

        if (bookToDelete == null) {
            return ResponseEntity.notFound().build();
        }
        this.bookService.deleteById(bookId);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Get a book by id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Book selected",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = BookGet.class))
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Book not found",
                    content = @Content
            )
    })
    @GetMapping("/{bookId}")
    public ResponseEntity<BookGet> getBookById(@PathVariable Long bookId) {
        if (this.bookService.findById(bookId) == null) {
            return ResponseEntity.notFound().build();
        }
        BookGet book = new BookGet(this.bookService.findById(bookId));
        return ResponseEntity.ok(book);
    }
}
