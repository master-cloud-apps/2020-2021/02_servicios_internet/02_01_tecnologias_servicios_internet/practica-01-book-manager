package org.eyo.bookmanager.controllers;

import org.eyo.bookmanager.models.Book;
import org.eyo.bookmanager.models.Comment;
import org.eyo.bookmanager.services.BookService;
import org.eyo.bookmanager.services.CommentService;
import org.eyo.bookmanager.services.UserSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HomeController {

    private BookService bookService;
    private CommentService commentService;
    private UserSession userSession;

    public HomeController(BookService bookService, CommentService commentService, UserSession userSession) {
        this.bookService = bookService;
        this.commentService = commentService;
        this.userSession = userSession;
    }

    @GetMapping(value = {"/index.html", ""})
    public String homePage(Model model) {
        model.addAttribute("books", this.bookService.getAll());
        return "home";
    }

    @PostMapping("/books/index.html")
    public String newPost(Model model, Book book) {

        this.bookService.save(book);
        model.addAttribute("book", book);

        return "book";
    }

    @PostMapping("/books/{bookId}/comments/index.html")
    public String newCommentOnBook(Model model, @PathVariable Long bookId, Comment comment) {

        this.commentService.save(bookId, comment);
        userSession.setUserName(comment.getName());
        model.addAttribute("book", this.bookService.findById(bookId));
        model.addAttribute("user_name", this.userSession.getUserName());

        return "book";
    }

    @GetMapping("/books/{bookId}/comments/{commentId}/delete.html")
    public String deleteCommentOnBook(Model model, @PathVariable Long bookId, @PathVariable Long commentId) {

        this.commentService.deleteComment(bookId, commentId);
        model.addAttribute("book", this.bookService.findById(bookId));
        model.addAttribute("user_name", this.userSession.getUserName());

        return "book";
    }

    @GetMapping("/books/{bookId}/index.html")
    public String bookHomePage(@PathVariable Long bookId, Model model) {
        model.addAttribute("book", this.bookService.findById(bookId));
        return "book";
    }


}
