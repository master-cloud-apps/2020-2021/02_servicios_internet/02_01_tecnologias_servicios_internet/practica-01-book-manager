package org.eyo.bookmanager.models;

public class Comment {
    private Long id;
    private String content;
    private String name;
    private Float punctuation;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Float getPunctuation() {
        return punctuation;
    }

    public void setPunctuation(Float punctuation) {
        this.punctuation = punctuation;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
