package org.eyo.bookmanager.models;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static java.util.stream.Collectors.toCollection;

public class Book {
    private String title;
    private String review;
    private String author;
    private String editorial;
    private Long yearPublication;
    private Long id = 0L;
    private ConcurrentMap<Long, Comment> comments = new ConcurrentHashMap<>();

    public Book(String title) {
        super();
        this.title = title;
    }

    public Book() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setComment(Comment comment) {
        this.comments.put(comment.getId(), comment);
    }

    public void deleteComment(Long commentId) {
        this.comments.remove(commentId);
    }

    public List<Comment> comments(){
        return this.comments.values().stream().collect(toCollection(ArrayList::new));
    }

    public String review() {
        return this.review;
    }

    public String author() {
        return this.author;
    }

    public String editorial() {
        return this.editorial;
    }

    public Long yearPublication() {
        return this.yearPublication;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public void setYearPublication(Long yearPublication) {
        this.yearPublication = yearPublication;
    }
}
