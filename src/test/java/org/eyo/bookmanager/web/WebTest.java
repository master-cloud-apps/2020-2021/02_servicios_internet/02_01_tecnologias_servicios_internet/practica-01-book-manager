package org.eyo.bookmanager.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public abstract class WebTest {
    @Autowired
    protected MockMvc mockMvc;

    protected Long getIdFromLocation(String locationHeader) {
        return Long.parseLong(locationHeader.substring(locationHeader.lastIndexOf('/') + 1));
    }

    protected Long createBook(String bookJson) throws Exception {
        return createEntityAndReturnId(bookJson, "/books");
    }

    protected Long createComment(String commentJson, Long bookId) throws Exception {
        return createEntityAndReturnId(commentJson, "/books/"+ bookId +"/comments");
    }

    protected Long createEntityAndReturnId(String elementJson, String uri) throws Exception {
        MvcResult result = this.mockMvc.perform(
                post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(elementJson))
                .andExpect(status().isCreated())
                .andReturn();
        return getIdFromLocation(result.getResponse().getHeader("location"));
    }
}
