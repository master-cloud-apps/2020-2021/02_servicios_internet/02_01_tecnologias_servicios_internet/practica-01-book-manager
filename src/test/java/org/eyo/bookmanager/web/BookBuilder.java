package org.eyo.bookmanager.web;

public class BookBuilder {

    private String title;
    private String review;
    private String author;
    private String editorial;
    private Long yearPublication;

    public BookBuilder setTitle(String title){
        this.title = title;
        return this;
    }

    public BookBuilder setReview(String review){
        this.review = review;
        return this;
    }

    public BookBuilder setAuthor(String author){
        this.author = author;
        return this;
    }

    public BookBuilder setEditorial(String editorial){
        this.editorial = editorial;
        return this;
    }

    public BookBuilder setYearPublication(Long yearPublication){
        this.yearPublication = yearPublication;
        return this;
    }

    public String build(){
        return "{" +
                "\"title\": \""+ this.title+"\"," +
                "\"review\": \""+ this.review+"\"," +
                "\"author\": \""+ this.author+"\"," +
                "\"editorial\": \""+ this.editorial+"\"," +
                "\"yearPublication\": "+ this.yearPublication+"" +
                "}";
    }
}
