package org.eyo.bookmanager.web;

public class CommentBuilder {

    private String content;
    private String name;
    private Float punctuation;

    public CommentBuilder setContent(String content){
        this.content = content;
        return this;
    }

    public CommentBuilder setName(String name){
        this.name = name;
        return this;
    }

    public CommentBuilder setPunctuation(Float punctuation){
        this.punctuation = punctuation;
        return this;
    }

    public String build(){
        return "{" +
                "\"content\": \""+ this.content+"\"," +
                "\"name\": \""+ this.name+"\"," +
                "\"punctuation\": "+ this.punctuation+"" +
                "}";
    }
}
