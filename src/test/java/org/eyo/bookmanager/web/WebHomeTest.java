package org.eyo.bookmanager.web;

import org.eyo.bookmanager.services.BookService;
import org.eyo.bookmanager.services.CommentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class WebHomeTest extends WebTest {

    @Autowired
    private BookService bookService;
    @Autowired
    private CommentService commentService;

    @Test
    void when_calling_home_page_should_not_return_any_book() throws Exception {
        this.mockMvc.perform(get("/index.html")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(not(containsString("Example"))));
    }

    @Test
    void given_one_book_saved_when_calling_home_page_should_return_book() throws Exception {
        Long bookId = createBook(new BookBuilder().setAuthor("test_author").setTitle("test_title").build());

        this.mockMvc.perform(get("/")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("test_title")));

        this.mockMvc.perform(delete("/books/" + bookId));
    }

    @Test
    void given_one_book_saved_when_calling_home_book_page_should_return_book() throws Exception {
        Long bookId = createBook(new BookBuilder()
                .setAuthor("test_author")
                .setTitle("test_title")
                .setYearPublication(1986L).build());

        this.mockMvc.perform(get("/books/" + bookId + "/index.html")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("test_title")));

        this.mockMvc.perform(delete("/books/" + bookId));
    }

    private String buildUrlEncodedFormEntity(String... params) {
        if ((params.length % 2) > 0) {
            throw new IllegalArgumentException("Need to give an even number of parameters");
        }
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < params.length; i += 2) {
            if (i > 0) {
                result.append('&');
            }
            try {
                result.
                        append(URLEncoder.encode(params[i], StandardCharsets.UTF_8.name())).
                        append('=').
                        append(URLEncoder.encode(params[i + 1], StandardCharsets.UTF_8.name()));
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
        }
        return result.toString();
    }

    @Test
    void given_book_created_by_form_should_return_ok() throws Exception {
        this.mockMvc.perform(
                post("/books/index.html")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .content(buildUrlEncodedFormEntity("title", "Book from form")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Book from form")));

        this.bookService.getAll().stream().forEach(book -> this.bookService.deleteById(book.getId()));
    }

    @Test
    void given_book_created_when_create_comment_by_form_should_return_ok() throws Exception {
        Long bookId = createBook(new BookBuilder()
                .setAuthor("test_author")
                .setTitle("test_title")
                .setYearPublication(1986L).build());

        this.mockMvc.perform(
                post("/books/" + bookId + "/comments/index.html")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .content(buildUrlEncodedFormEntity("name", "Alberto",
                                "content", "comment for test",
                                "punctuation", "4.5")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("comment for test")));

        this.bookService.getAll().stream().forEach(book -> this.bookService.deleteById(book.getId()));
    }

    @Test
    void given_book_and_comment_created_when_delete_comment_by_html_should_return_ok() throws Exception {
        Long bookId = createBook(new BookBuilder()
                .setAuthor("test_author")
                .setTitle("test_title")
                .setYearPublication(1986L).build());
        Long commentId =
                createComment(new CommentBuilder().setContent("test_content").setName("Alberto").setPunctuation(4.5F).build(), bookId);

        this.mockMvc.perform(get("/books/" + bookId + "/comments/" + commentId + "/delete.html"))
                .andDo(print())
                .andExpect(status().isOk());

        this.commentService.getAll(bookId).stream()
                .forEach(comment -> this.commentService.deleteComment(bookId, comment.getId()));
        this.bookService.getAll().stream().forEach(book -> this.bookService.deleteById(book.getId()));
    }
}
