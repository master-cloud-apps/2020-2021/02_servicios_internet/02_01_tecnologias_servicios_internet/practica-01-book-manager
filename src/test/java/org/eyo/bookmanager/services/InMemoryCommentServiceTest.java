package org.eyo.bookmanager.services;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InMemoryCommentServiceTest {

    private CommentService commentService = new InMemoryCommentService(new InMemoryBookService());

    @Test
    void should_get_empty_comments_list(){
        assertEquals(0, this.commentService.getAll(-1L).size());
    }
}
